﻿var PrinterLoader = function(require, exports, module) {

    var exec = require("cordova/exec");

    function Printer(require, exports, module) {

        // 打印对齐方式
        this.PrintType = {
            Left: "Left",
            Centering: "Centering",
            VerticalCentering: "VerticalCentering",
            VerticalHorizontalCentering: "VerticalHorizontalCentering",
            TopCentering: "TopCentering",
            LeftTop: "LeftTop",
            RightTop: "RightTop",
            Right: "Right"
        };
    }

    // 打开打印机
    Printer.prototype.open = function(successCallback, errorCallback) {

        if (errorCallback == null) {
            errorCallback = function() {};
        }
        
        if (successCallback == null) {
            successCallback = function() {};
        }

        if (typeof errorCallback != "function") {
            console.log("Printer.print failure: failure parameter not a function");
            return;
        }

        if (typeof successCallback != "function") {
            console.log("Printer.print failure: success callback parameter must be a function");
            return;
        }

        exec(successCallback, errorCallback, 'Printer', 'open', []);
    };

    // 关闭打印机
    Printer.prototype.close = function(successCallback, errorCallback) {

        if (errorCallback == null) {
            errorCallback = function() {};
        }
        
        if (successCallback == null) {
            successCallback = function() {};
        }

        if (typeof errorCallback != "function") {
            console.log("Printer.print failure: failure parameter not a function");
            return;
        }

        if (typeof successCallback != "function") {
            console.log("Printer.print failure: success callback parameter must be a function");
            return;
        }

        exec(successCallback, errorCallback, 'Printer', 'close', []);
    };
    
    // 进纸
    Printer.prototype.step = function(successCallback, errorCallback) {

        if (errorCallback == null) {
            errorCallback = function() {};
        }
        
        if (successCallback == null) {
            successCallback = function() {};
        }

        if (typeof errorCallback != "function") {
            console.log("Printer.print failure: failure parameter not a function");
            return;
        }

        if (typeof successCallback != "function") {
            console.log("Printer.print failure: success callback parameter must be a function");
            return;
        }

        exec(successCallback, errorCallback, 'Printer', 'step', []);
    };

    // 开始一行
    // 入参
    // 	lineHeight：行高
    Printer.prototype.lineInit = function(lineHeight,successCallback, errorCallback) {

        if (errorCallback == null) {
            errorCallback = function() {};
        }
        
        if (successCallback == null) {
            successCallback = function() {};
        }

        if (typeof errorCallback != "function") {
            console.log("Printer.print failure: failure parameter not a function");
            return;
        }

        if (typeof successCallback != "function") {
            console.log("Printer.print failure: success callback parameter must be a function");
            return;
        }

        exec(successCallback, errorCallback, 'Printer', 'PrintLineInit', [lineHeight]);
    };

    // 结束当前行
    Printer.prototype.lineEnd = function(successCallback, errorCallback) {

        if (errorCallback == null) {
            errorCallback = function() {};
        }
        
        if (successCallback == null) {
            successCallback = function() {};
        }

        if (typeof errorCallback != "function") {
            console.log("Printer.print failure: failure parameter not a function");
            return;
        }

        if (typeof successCallback != "function") {
            console.log("Printer.print failure: success callback parameter must be a function");
            return;
        }

        exec(successCallback, errorCallback, 'Printer', 'PrintLineEnd', []);
    };

    // 打印字串,需要调用 lineInit() 和 lineEnd()
    // 入参
    // 	content:打印的内容 , fontSize:文字大小 ， printType : 对齐方式
    Printer.prototype.printString = function(content, fontSize, printType,successCallback, errorCallback) {

        if (errorCallback == null) {
            errorCallback = function() {};
        }
        
        if (successCallback == null) {
            successCallback = function() {};
        }

        if (typeof errorCallback != "function") {
            console.log("Printer.print failure: failure parameter not a function");
            return;
        }

        if (typeof successCallback != "function") {
            console.log("Printer.print failure: success callback parameter must be a function");
            return;
        }

        exec(successCallback, errorCallback, 'Printer', 'PrintLineString', [content, fontSize, printType]);
    };

    // 打印字串的扩展方法，不需要调用 lineInit() 和 lineEnd()
    // 入参
    // 	content:打印的内容 , fontSize:文字大小 ， printType : 对齐方式
    Printer.prototype.printStringEx = function(content, fontSize, printType,successCallback, errorCallback) {

        if (errorCallback == null) {
            errorCallback = function() {};
        }
        
        if (successCallback == null) {
            successCallback = function() {};
        }

        if (typeof errorCallback != "function") {
            console.log("Printer.print failure: failure parameter not a function");
            return;
        }

        if (typeof successCallback != "function") {
            console.log("Printer.print failure: success callback parameter must be a function");
            return;
        }

        exec(successCallback, errorCallback, 'Printer', 'PrintStringEx', [content, fontSize, printType]);
    };

    // 打印条码（默认#39格式）
    // 入参
    // 	content:条码内容 , width:宽度 , height:高度
    Printer.prototype.printBarCode = function(content, width, height,successCallback, errorCallback) {

        if (errorCallback == null) {
            errorCallback = function() {};
        }
        
        if (successCallback == null) {
            successCallback = function() {};
        }

        if (typeof errorCallback != "function") {
            console.log("Printer.print failure: failure parameter not a function");
            return;
        }

        if (typeof successCallback != "function") {
            console.log("Printer.print failure: success callback parameter must be a function");
            return;
        }

        exec(successCallback, errorCallback, 'Printer', 'PrintBarCode', [content, width, height]);
    };

    // 打印二维码
    // 入参
    // 	content:条码内容 , width:宽度 , height:高度
    Printer.prototype.printQRCode = function( content, width, height,successCallback, errorCallback) {

        if (errorCallback == null) {
            errorCallback = function() {};
        }
        
        if (successCallback == null) {
            successCallback = function() {};
        }

        if (typeof errorCallback != "function") {
            console.log("Printer.print failure: failure parameter not a function");
            return;
        }

        if (typeof successCallback != "function") {
            console.log("Printer.print failure: success callback parameter must be a function");
            return;
        }

        exec(successCallback, errorCallback, 'Printer', 'PrintQRCode', [content, width, height]);
    };
    
    // 添加字符串
    Printer.prototype.addString = function(content,size,orie,successCallback, errorCallback){
    	
        if (errorCallback == null) {
            errorCallback = function() {};
        }
        
        if (successCallback == null) {
            successCallback = function() {};
        }

        if (typeof errorCallback != "function") {
            console.log("Printer.print failure: failure parameter not a function");
            return;
        }

        if (typeof successCallback != "function") {
            console.log("Printer.print failure: success callback parameter must be a function");
            return;
        }

        exec(successCallback, errorCallback, 'Printer', 'addString', [content, size, orie]);
    }
    
    // 添加条形码
    Printer.prototype.addBarCode = function(content,width,height,successCallback, errorCallback){
    	
        if (errorCallback == null) {
            errorCallback = function() {};
        }
        
        if (successCallback == null) {
            successCallback = function() {};
        }

        if (typeof errorCallback != "function") {
            console.log("Printer.print failure: failure parameter not a function");
            return;
        }

        if (typeof successCallback != "function") {
            console.log("Printer.print failure: success callback parameter must be a function");
            return;
        }

        exec(successCallback, errorCallback, 'Printer', 'addBarCode', [content, width, height]);
    }
    
    // 添加二维码
    Printer.prototype.addQRCode = function(content,width,height,successCallback, errorCallback){
    	
        if (errorCallback == null) {
            errorCallback = function() {};
        }
        
        if (successCallback == null) {
            successCallback = function() {};
        }

        if (typeof errorCallback != "function") {
            console.log("Printer.print failure: failure parameter not a function");
            return;
        }

        if (typeof successCallback != "function") {
            console.log("Printer.print failure: success callback parameter must be a function");
            return;
        }

        exec(successCallback, errorCallback, 'Printer', 'addQRCode', [content, width, height]);
    }
    
    // 将添加的内容打印出来，调用此方法后，之前add的内容在打印完成后将被清空
    Printer.prototype.print = function(successCallback, errorCallback){
    	
        if (errorCallback == null) {
            errorCallback = function() {};
        }
        
        if (successCallback == null) {
            successCallback = function() {};
        }

        if (typeof errorCallback != "function") {
            console.log("Printer.print failure: failure parameter not a function");
            return;
        }

        if (typeof successCallback != "function") {
            console.log("Printer.print failure: success callback parameter must be a function");
            return;
        }

        exec(successCallback, errorCallback, 'Printer', 'print', []);
    }

    var printer = new Printer();
    module.exports = printer;

};

PrinterLoader(require, exports, module);

cordova.define("cordova/plugin/Printer", PrinterLoader);