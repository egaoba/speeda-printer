package com.speeda.pda.print;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.ShortBuffer;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import org.apache.cordova.CordovaWebView;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CordovaInterface;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.FontMetrics;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.BinaryBitmap;
import com.google.zxing.EncodeHintType;
import com.google.zxing.LuminanceSource;
import com.google.zxing.MultiFormatReader;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.Result;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.common.HybridBinarizer;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import hardware.print.printer;
import hardware.print.printer.PrintType;

import static com.speeda.pda.print.Printer.PrintContentType.STRING;

public class Printer extends CordovaPlugin {

	printer m_printer = new printer();

	List<PrintContent> conts = new ArrayList<PrintContent>();
	boolean isOpen = false;

	/**
	 *
	 * @param action          The action to execute.
	 * @param args            The exec() arguments.
	 * @param callbackContext The callback context used when calling back into JavaScript.
	 * @return
	 * @throws JSONException
	 */
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {

		// 打开打印机
		if ("open".equals(action)) {
			if(!isOpen) m_printer.Open();
			isOpen = true;
			return true;
		}
		
		// 关闭打印机
		if ("close".equals(action)) {
			m_printer.Close();
			return true;
		}

		if("addString".equals(action)){

			PrintContent pc = new PrintContent();
			pc.contentType = STRING;
			pc.content = args.getString(0);
			pc.size = args.getInt(1);
			pc.printType = PrintType.valueOf(args.getString(2));
			conts.add(pc);

		}

		if("addBarCode".equals(action)){
			PrintContent pc = new PrintContent();
			pc.contentType = PrintContentType.BARCODE;
			pc.content = args.getString(0);
			pc.width = args.getInt(1);
			pc.height = args.getInt(2);
			conts.add(pc);

		}

		if("addQRCode".equals(action)){
			PrintContent pc = new PrintContent();
			pc.contentType = PrintContentType.QRCODE;
			pc.content = args.getString(0);
			pc.width = args.getInt(1);
			pc.height = args.getInt(2);
			conts.add(pc);

		}

		if("print".equals(action)){



			if(!this.isOpen) m_printer.Open();

			//m_printer.PrintStringEx("数量：" + conts.size(), 20, false, true, PrintType.Left);

			for(PrintContent pc : conts){

				switch (pc.contentType){
					case STRING:
						//m_printer.PrintStringEx(pc.content, pc.size, false, true, pc.printType);
						m_printer.PrintLineInit(pc.size);
						m_printer.PrintLineString(pc.content, pc.size, pc.printType,true);
						m_printer.PrintLineEnd();
						break;
					case BARCODE:
						this.PrintBarCode(pc.content,pc.width,pc.height);
						break;
					case QRCODE:
						this.PrintQRCode(pc.content,pc.width,pc.height);
						break;

				}

			}

			conts.clear();


		}

		// 开始新的行
		if ("PrintLineInit".equals(action)) {
			m_printer.PrintLineInit(args.getInt(0));
			return true;
		}

		// 结束当前行
		if ("PrintLineEnd".equals(action)) {
			m_printer.PrintLineEnd();
			return true;
		}

		// 打印扩展字符串
		if ("PrintStringEx".equals(action)) {

			PrintType pt = PrintType.valueOf(args.getString(2));

			if (pt == null) {
				return false;
			}

			m_printer.PrintStringEx(args.getString(0), args.getInt(1), false, true, pt);
			
			return true;
		}

		// 打印字符串
		if ("PrintLineString".equals(action)) {

			PrintType pt = PrintType.valueOf(args.getString(2));

			if (pt == null) {
				return false;
			}

			m_printer.PrintLineString(args.getString(0), args.getInt(1), pt, true);
			
			return true;
		}

		// 出纸
		if ("step".equals(action)) {
			m_printer.Step((byte) 0xff);
			return true;
		}

		// 打印条形码
		if ("PrintBarCode".equals(action)) {

			return PrintBarCode(args.getString(0),args.getInt(1),args.getInt(1));
		}

		// 打印二维码
		if ("PrintQRCode".equals(action)) {

			return PrintQRCode(args.getString(0),args.getInt(1),args.getInt(1));

		}

		return false;
	}

	public boolean PrintBarCode(String content,int width,int height){
		Bitmap bitMap = null;

		try{
			bitMap = this.createBarCode(content,width, height);
		}catch(Exception ex){
			//callbackContext.error(ex.toString());
		}

		if (bitMap == null) {
			return false;
		} else {
			m_printer.PrintBitmap(bitMap);
		}

		return true;
	}

	public boolean PrintQRCode(String content,int width,int height){

		Bitmap bitMap = null;

		try{
			bitMap = this.createQRCode(content, width, height);
		}catch(Exception ex){
			//callbackContext.error(ex.toString());
		}

		if (bitMap == null) {
			return false;
		} else {
			m_printer.PrintBitmap(bitMap);

		}

		return true;
	}

	public Bitmap createBarCode(String content, int width, int height)  throws Exception{

		int codeWidth = 3 + // start guard
				(7 * 6) + // left bars
				5 + // middle guard
				(7 * 6) + // right bars
				3; // end guard

		codeWidth = Math.max(codeWidth, width);
		//try {
			BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.CODE_39, codeWidth, height,
					null);

			return getBitmap(bitMatrix);

		//} catch (Exception e) {
		//	return null;
		//}
	}

	public Bitmap createQRCode(String content, int width, int height)  throws Exception {

		Map<EncodeHintType, Object> hints = new Hashtable<EncodeHintType, Object>();

		hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.L);

		hints.put(EncodeHintType.CHARACTER_SET, "GBK");

		//try {
			BitMatrix bitMatrix = new MultiFormatWriter().encode(content, BarcodeFormat.QR_CODE, width, height, hints);

			return getBitmap(bitMatrix);

		//} catch (Exception e) {
		//	return null;
		//}

	}

	private Bitmap getBitmap(BitMatrix matrix) {
		final int WHITE = 0xFFFFFFFF;
		final int BLACK = 0xFF000000;

		int width = matrix.getWidth();
		int height = matrix.getHeight();
		int[] pixels = new int[width * height];
		for (int y = 0; y < height; y++) {
			int offset = y * width;
			for (int x = 0; x < width; x++) {
				pixels[offset + x] = matrix.get(x, y) ? BLACK : WHITE;
			}
		}

		Bitmap bitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
		bitmap.setPixels(pixels, 0, width, 0, 0, width, height);
		return bitmap;
	}

	public enum  PrintContentType{
		STRING,
		BARCODE,
		QRCODE
	}

	public class PrintContent{
		public PrintContentType contentType;
		public String content;
		public int width;
		public int height;
		public int size;
		public PrintType printType;

	}

}